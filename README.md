# CPSC 583 (Introduction to Information Visualization) at the University of Calgary Term Project
### By: Elzanne Venter

## Purpose

<div>The purpose of this project was to produce a visualization that would communicate information about different countries involving various measures of quality of life. This visualization could be used to identify how quality of life could be improved in many places, by examining where it is currently better and identifying the factors responsible. Ultimately, the goal of the visualization was to effectively communicate how the different countries compared to one another in the different measures of quality of life. </div>

## The Data

<div>The data set I chose for my project contains information about the average life expectancy of people in different countries for every year from 2000-2015, as well as information related to healthcare and diseases etc., in these countries. For my project I focused only on the data for 2015. Additionally, the columns I used are Country, Life Expectancy, BMI, GDP, Income composition of resources, and Schooling. The ‘Country’ column represents the country the data is from. The ‘Life Expectancy’ column is the average life expectancy in years. The ‘BMI’ column is the average body mass index of the people in the country.  The ‘GDP’ column is the Gross Domestic Product per capita in US dollars. The ‘Income composition of resources’ column is the “Human Development Index in terms of income composition of resources (index ranging from 0 to 1)” [https://www.kaggle.com/kumarajarshi/life-expectancy-who]. Schooling represents the average number of years of school attended.</div>

## Final Result

<div>The final interactive visualization is available [here](https://elzanne1.github.io/583FinalProj/) </div> 
<div>Screenshots and interactions shown below</div>
<div> <img src = "./images/Picture1.png"></img></div>
<div> <img src = "./images/Picture2.png"></img></div>
<div> <img src = "./images/Picture3.png"></img></div>
<div> <img src = "./images/Picture4.png"></img></div>

